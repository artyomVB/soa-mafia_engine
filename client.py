import threading
import grpc
import mafia_pb2
import mafia_pb2_grpc


class Client:
    def __init__(self, name):
        self.id = 0
        self.name = name
        self.role = None
        self.current_conected_players = []
        self.channel1 = grpc.insecure_channel('localhost:8080')
        self.channel2 = grpc.insecure_channel('localhost:8080')

    def start_night(self, stub, alive):
        print("NIGHT")
        print("Alive players", alive)
        if self.role == "killed" or self.role == "civilian":
            response = stub.SkipNight(mafia_pb2.Empty())
        elif self.role == "mafia":
            print("Choose player to kill")
            response = stub.KillPlayerMafia(mafia_pb2.KillPlayerMafiaRequest(id=int(input())))
        else:
            print("Choose player to check")
            response = stub.CheckPlayer(mafia_pb2.CheckPlayerRequest(id=int(input())))
        print("END NIGHT")
        if response.flag:
            print("Mafia:", response.checked)
        print("Killed:", response.killed)
        del alive[response.killed]
        if self.id == response.killed:
            self.role = "killed"
        if response.end_game:
            print("\nMafia won!\n")
            return
        self.start_day(stub, alive)

    def start_day(self, stub, alive):
        print("DAY\n")
        if self.role == "killed":
            response = stub.EndDay(mafia_pb2.EndDayRequest(id=self.id))
            del alive[response.killed]
            self.start_night(stub, alive)
        print("Alive players:", alive)
        print("Enter player id to vote for him")
        stub.KillPlayer(mafia_pb2.KillRequest(id=int(input())))
        print("Press any key to end the day")
        input()
        response = stub.EndDay(mafia_pb2.EndDayRequest(id=self.id))
        if response.end_game:
            print("\nCivilians won!\n")
            return
        del alive[response.killed]
        if self.id == response.killed:
            self.role = "killed"
        self.start_night(stub, alive)

    def get_event(self):
        stub = mafia_pb2_grpc.MafiaEngineStub(self.channel2)
        for event in stub.SubscribeOnNotifications(mafia_pb2.SubscribeRequest(id=self.id)):
            if event.type == "CONNECT":
                print("Connected:", event.name)
                self.current_conected_players.append(event.name)
            else:
                print("Disconnected:", event.name)
                self.current_conected_players.remove(event.name)
        self.close_sec()

    def run(self):
        stub = mafia_pb2_grpc.MafiaEngineStub(self.channel1)
        response = stub.SetName(mafia_pb2.SetNameRequest(name=self.name))
        self.id = response.id
        self.current_conected_players = response.names
        th = threading.Thread(target=self.get_event, args=())
        th.start()
        while True:
            print("Type d to disconnect\nType c to see who is online\nType r if ready")
            symb = input()
            if symb == 'd':
                stub.Disconnect(mafia_pb2.DisconnectRequest(id=self.id))
                break
            elif symb == 'c':
                print(self.current_conected_players)
            else:
                response = stub.SetReadyStatus(mafia_pb2.ReadyRequest(id=self.id))
                print("role:", response.role)
                self.role = response.role
                self.start_day(stub, dict(zip(response.ids, response.players)))
                break

        self.close_first()

    def close_first(self):
        self.channel1.close()

    def close_sec(self):
        self.channel2.close()


if __name__ == '__main__':
    name = input()
    client = Client(name)
    client.run()
