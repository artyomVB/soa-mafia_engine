import queue
import threading

import mafia_pb2
import mafia_pb2_grpc
from concurrent import futures
from random import shuffle
import grpc


class Server(mafia_pb2_grpc.MafiaEngineServicer):
    def __init__(self):
        self.roles = ["mafia", "sherif", "civilian", "civilian"]
        shuffle(self.roles)

        self.queues = {}
        self.id_to_role = {}
        self.id_to_name = {}
        self.curr_id = 0
        self.cv = threading.Condition()
        self.ready = 0
        self.votes = {}
        self.end_game = False
        self.counter_killed_civilians = 0

        self.flag = None
        self.killed = None
        self.checked = None


    def ClearVotes(self):
        for i in self.votes.keys():
            self.votes[i] = 0

    def MostVotes(self):
        mx = max(self.votes.values())
        for i in self.votes.keys():
            if self.votes[i] == mx:
                return i
        return None

    def CountCivilians(self):
        c = 0
        for elem in self.id_to_role.values():
            if elem == "civilian" or elem == "sherif":
                c += 1
        return c

    def SetName(self, request, context):
        self.curr_id += 1
        self.id_to_name[self.curr_id] = request.name
        arr = []
        for i in self.queues:
            self.queues[i].put((request.name, "CONNECT"))
            arr.append(self.id_to_name[i])
        self.queues[self.curr_id] = queue.Queue()
        return mafia_pb2.ConnectedPlayers(names=arr, id=self.curr_id)

    def SubscribeOnNotifications(self, request, context):
        while True:
            name, type = self.queues[request.id].get()
            if name is None:
                break
            yield mafia_pb2.SubscribeResponse(name=name, type=type)

    def Disconnect(self, request, context):
        self.queues[request.id].put((None, None))
        del self.queues[request.id]
        for elem in self.queues:
            self.queues[elem].put((self.id_to_name[request.id], "DISCONNECT"))
        del self.id_to_name[request.id]
        return mafia_pb2.Empty()

    def SetReadyStatus(self, request, context):
        self.ready += 1
        self.votes[request.id] = 0
        with self.cv:
            while self.ready % 4 != 0:
                self.cv.wait()
            role = self.roles.pop()
            self.id_to_role[request.id] = role
            self.cv.notify()
        return mafia_pb2.ReadyResponse(role=role, players=self.id_to_name.values(), ids=self.id_to_name.keys())

    def EndDay(self, request, context):
        self.flag = None
        self.killed = None
        self.ready += 1
        with self.cv:
            while self.ready % 4 != 0:
                self.cv.wait()
            self.cv.notify_all()
        if self.id_to_role[self.MostVotes()] == "mafia":
            return mafia_pb2.EndDayResponse(killed=self.MostVotes(), end_game=True)
        self.id_to_role[self.MostVotes()] = "killed"
        return mafia_pb2.EndDayResponse(killed=self.MostVotes(), end_game=False)

    def KillPlayer(self, request, context):
        self.votes[request.id] += 1
        return mafia_pb2.Empty()

    def SkipNight(self, request, context):
        self.ready += 1
        with self.cv:
            while self.ready % 4 != 0:
                self.cv.wait()
            self.cv.notify_all()
        return mafia_pb2.EndNightResponse(killed=self.killed, flag=self.flag, checked=self.checked, end_game=self.end_game)

    def KillPlayerMafia(self, request, context):
        self.ready += 1
        self.killed = request.id
        self.id_to_role[request.id] = "killed"
        if self.CountCivilians() <= 1:
            self.end_game = True
        with self.cv:
            while self.ready % 4 != 0:
                self.cv.wait()
            self.cv.notify_all()
        return mafia_pb2.EndNightResponse(killed=self.killed, flag=self.flag, checked=self.checked, end_game=self.end_game)

    def CheckPlayer(self, request, context):
        self.ready += 1
        self.flag = self.id_to_role[request.id] == "mafia"
        self.checked = request.id
        with self.cv:
            while self.ready % 4 != 0:
                self.cv.wait()
            self.cv.notify_all()
        return mafia_pb2.EndNightResponse(killed=self.killed, flag=self.flag, checked=self.checked, end_game=self.end_game)


def serve() -> None:
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    mafia_pb2_grpc.add_MafiaEngineServicer_to_server(Server(), server)
    listen_addr = '[::]:8080'
    server.add_insecure_port(listen_addr)
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    serve()
